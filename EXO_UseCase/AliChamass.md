User Story

US1    En tant que client, je veux consulter le catalogue des produits afin de pouvoir choisir un produit.

US2    En tant que client, je veux enregistrer un achat afin de pouvoir revenir le voir plus tard.

US3    En tant que client, je veux m’authentifier afin de pouvoir enregistrer mes informations.

US4    En tant que client, je veux constituer un panier pour pouvoir acheter plusieurs articles en même temps

UST5    En tant que technicien, je veux consulter les remarques afin de pouvoir améliorer nos services.


