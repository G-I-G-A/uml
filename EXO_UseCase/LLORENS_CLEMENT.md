USERSTORY CLEMENT LLORENS


US1    En tant que client , je veux consulter le catalogue de produit afin de choisir un produit.

US2    En tant que client , je veux pouvoir constituer un panier afin de pouvoir passer un achat.

US3    En tant que client , je veux pouvoir m’authentifier afin d’avoir un profil client.

US3    En tant que client , je veux pouvoir enregistrer un règlement afin de pouvoir garder mes informations pour mes futurs achats.

US4    En tant que client , je veux pouvoir saisir mes informations de livraisons afin d’être livré.

UST7    En tant que Technicien, je veux pouvoir consulter une remarque afin de corriger les erreurs.